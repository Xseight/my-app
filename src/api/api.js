import axios from 'axios';

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        "API-KEY": "d9e4bdf4-e795-4fe6-a0e9-a25e5d59ffaa"
    },
})

export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 10) {

        return instance.get(`users?page=${currentPage}&count=${pageSize}`)
            .then(response => {
                return response.data;
            });
    },

     deleteFriend(userId) {
        return instance.delete(`follow/${userId}`)
     },

     addFriend(userId) {
        return instance.post(`follow/${userId}`)
     }
}

export const authAPI = {
    getAuthUserData() {
        return instance.get(`auth/me`);
    },

    login(email, password, rememberMe = false) {
        return instance.post(`auth/login`, {email, password, rememberMe});
    },

    logout(email, password, rememberMe = false) {
        return instance.delete(`auth/login`);
    }
}

export const profileAPI = {
    getUserProfile(userId) {
        return instance.get('profile/'+userId) //запрос профиля
    },

    getUserStatus(userId) {
        return instance.get('profile/status/'+userId) //Запрос статуса
    },

    updateUserStatus(status) {
        return instance.put('profile/status', {status: status}) //Апдейт статуса
    }
}
