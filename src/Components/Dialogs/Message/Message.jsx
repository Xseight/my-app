import React from 'react';
import prom from './../Dialogs.module.css';


const Message = (props) => {
    return (
        <div className={prom.message}>
            {props.message}
        </div>
    )
}

export default Message;