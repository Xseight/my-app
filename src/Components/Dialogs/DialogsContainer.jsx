import React from 'react';
import { sendMessageAC} from '../../Redux/Dialogs-Reducer';
import Dialogs from './Dialogs';
import {connect} from 'react-redux';
import { withAuthRedirect } from '../../hoc/Auth-Redirect';
import { compose } from 'redux';


let mapStateToProps = (state) => {
return {
    dialogsPage: state.dialogsPage
}
}

let mapDispatchToProps = (dispatch) => {
    return {
        sendMessage: (newMessageText) =>    {
            dispatch(sendMessageAC(newMessageText));
        }
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withAuthRedirect
)(Dialogs);

