import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { required, maxLengthCreator } from '../../utils/validators/validators';
import { Textarea } from '../common/FormsControl/FormsControls';
import styles from './Dialogs.module.css';

let maxLength100 = maxLengthCreator(100);

const AddMessageForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={Textarea}
                    name={"newMessageText"}
                    placeholder={'Введите Сообщение...'}
                    validate={[required, maxLength100]} />
            </div>
            <div>
                <button className={styles.set_button}>Отправить</button>
            </div>
        </form>
    )
}

export default reduxForm({form: 'addMessageForm' })(AddMessageForm)
