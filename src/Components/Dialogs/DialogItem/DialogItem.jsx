import React from 'react';
import { NavLink } from 'react-router-dom';
import prom from './../Dialogs.module.css';
// import DialogsLink from './DialogsLink';

const DialogItem = (props) => {
    return (
        <div className={prom.dialogsItem + ' ' + prom.active}>
            <NavLink to={"/message/" + props.id}>{props.name}</NavLink>
        </div>
    )
}



export default DialogItem;