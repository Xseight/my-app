import React from 'react';
import DialogItem from './DialogItem/DialogItem';
import styles from './Dialogs.module.css';
import Message from './Message/Message';
import AddMessageForm from './AddMessageForm';

const Dialogs = (props) => {

    let state = props.dialogsPage;

    let DialogsElements = state.dialogs.map(d => <DialogItem name={d.name} key={d.id} id={d.id} />);
    let MessagesElement = state.messages.map(m => <Message message={m.message} key={m.id} />);
    // let newMessageText = state.newMessageText;

    let addNewMessage = (values) => {
        props.sendMessage(values.newMessageText);
    }

    return (
        <div className={styles.dialogs}>
            <div className={styles.dialogsItems}>
                {DialogsElements}
            </div>
            <div className={styles.messages}>
                <div>
                    {MessagesElement}
                </div>
               <AddMessageForm onSubmit={addNewMessage}/> 
            </div>

        </div>
    )
}

export default Dialogs;

