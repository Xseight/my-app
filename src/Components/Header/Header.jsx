import React from 'react';
import styles from './Header.module.css';
import {Link} from 'react-router-dom';

const Header = (props) => {
    return (
        <header className={styles.header}>
            <img src="https://logopond.com/assets/img/logo-footer.png" />
            <div className={styles.loginBlock}>
                {props.isAuth 
                ? <div>{props.login} - <button onClick={props.logout}>Выйти</button></div>
                : <Link to={'/login'}>Авторизоваться</Link>}
            </div>
        </header>
    )
}

export default Header;