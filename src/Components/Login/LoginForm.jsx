import React from 'react';
import { Field, reduxForm } from 'redux-form';
import styles from "./../common/FormsControl/FormsControl.module.css"
import { required } from '../../utils/validators/validators';
import { Input } from '../common/FormsControl/FormsControls';


const LoginForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field placeholder={'Email'} name={"email"} component={Input} validate={[required]} />
            </div>
            <div>
                <Field placeholder={'Пароль'} name={"password"} component={Input} validate={[required]} type={"password"} />
            </div>
            <div>
                <Field type={'checkbox'} name={"rememberMe"} component={Input} />Не выходить
            </div>
            {props.error && <div className={styles.formSummaryError}>
                {props.error}
            </div>}
            <div>
                <button>Войти</button>
            </div>
        </form>
    )
}

const LoginReduxForm = reduxForm({
    form: 'login'
})(LoginForm)

export default LoginReduxForm;