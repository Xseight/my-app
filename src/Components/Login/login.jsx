import React from 'react';
import { connect } from 'react-redux';
import LoginReduxForm from './LoginForm';
import {login} from '../../Redux/auth-reducer';
import { Navigate } from 'react-router-dom';


const Login = (props) => {
    const onSubmit = (formData) => {
        props.login(formData.email , formData.password, formData.rememberMe)
    }

    if (props.isAuth) {
        return <Navigate to={"/profile"}/>
    }
    return (
        <div>
            <div>Авторизация</div>
            <div>
                <LoginReduxForm onSubmit={onSubmit}/>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})
export default connect(mapStateToProps, {login})(Login);