import React from 'react';
import { NavLink } from 'react-router-dom';
import prom from './Navbar.module.css';

const NavLinkWrap = ({ children, link }) => {
    return (
      <div className={prom.item}>
        <NavLink to={link} className={({ isActive }) => isActive ? prom.active : ''}>
          { children }
        </NavLink>
      </div>
    )
  }

  export default NavLinkWrap;