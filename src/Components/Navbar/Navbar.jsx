import React from 'react';
import NavLinkWrap from './NavLinkWrap';
import prom from './Navbar.module.css';


const Navbar = () => {
  return (
    <nav className={prom.nav}>
      <div className={prom.navButton}>
      <NavLinkWrap link="/profile">Профиль</NavLinkWrap>
      </div>
      <div className={prom.navButton}>
      <NavLinkWrap link="/message">Сообщения</NavLinkWrap>
      </div>
      <div className={prom.navButton}>
      <NavLinkWrap link="/users">Пользователи</NavLinkWrap>
      </div>
      <div className={prom.navButton}>
      <NavLinkWrap link="/music">Музыка</NavLinkWrap>
      </div>
      <div className={prom.navButton}>
      <NavLinkWrap link="/settings">Настройки</NavLinkWrap>
      </div>
    </nav>
  )
}



export default Navbar;