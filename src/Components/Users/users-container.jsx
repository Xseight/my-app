import React from 'react';
import Users from './users';
import { connect } from 'react-redux';
import {addFriend, deleteFriend, setCurrentPage, toggleFollowingInProgress, getUsers} from '../../Redux/Users-Reducer';
import Preloader from '../common/Preloader/Preloader'
import { withAuthRedirect } from '../../hoc/Auth-Redirect';
import { compose } from 'redux';

class UsersContainer extends React.Component {

  componentDidMount() {
    this.props.getUsers(this.props.currentPage, this.props.pageSize);
  }

  onPageChanged = (pageNumber) => {
    this.props.getUsers(pageNumber, this.props.pageSize);
  }

  render() {

    return <>
      {this.props.isFetching ? <Preloader /> : null}
      <Users totalUserCount={this.props.totalUserCount}
        pageSize={this.props.pageSize}
        currentPage={this.props.currentPage}
        onPageChanged={this.onPageChanged}
        users={this.props.users}
        deleteFriend={this.props.deleteFriend}
        addFriend={this.props.addFriend}
        followingInProgress={this.props.followingInProgress}
      />
    </>

  }
}

let mapStateToProps = (state) => {
  return {
    users: state.usersPage.users,
    pageSize: state.usersPage.pageSize,
    totalUserCount: state.usersPage.totalUserCount,
    currentPage: state.usersPage.currentPage,
    isFetching: state.usersPage.isFetching,
    followingInProgress: state.usersPage.followingInProgress
  }
}

export default compose(
  connect(mapStateToProps, {addFriend, deleteFriend, setCurrentPage, toggleFollowingInProgress, getUsers, }),
)(UsersContainer);
