import React from 'react';
import { Field, reduxForm } from 'redux-form'
import { required, maxLengthCreator } from '../../../utils/validators/validators';
import Textarea from '../../common/FormsControl/FormsControls';
import styles from './MyPosts.module.css';

let maxLength10 = maxLengthCreator(10);

const AddPostsForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={Textarea}
                    className={styles.textarea_set}
                    name="newPostText"
                    placeholder="Введите текст..."
                    validate={[required, maxLength10]} />
            </div>
            <div className={styles.postButton}>
                <button className={styles.postButton_set}>Добавить</button>
            </div>
        </form>
    )
}
const AddPostsReduxForm = reduxForm({
    form: 'newPostText'
})(AddPostsForm)

export default AddPostsReduxForm;