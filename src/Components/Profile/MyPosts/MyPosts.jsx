import React from 'react';
import AddPostsReduxForm from './AddPostsForm';
import styles from './MyPosts.module.css';
import Post from './Post/Post';

const MyPosts = (props) => {

  let PostsElement = props.posts.map(p => <Post postText={p.postText} likeCount={p.likesCount} />);

  let addNewPost = (values) => {
    props.addPost(values.newPostText);
  };

  return (
    <div className={styles.myPostsBlock}>
      <div className={styles.editBlock}>
        <h3>Мои посты</h3>
        <div>
          <AddPostsReduxForm onSubmit={addNewPost} />
        </div>
      </div>
      <div className={styles.posts}>
        {PostsElement.reverse()}
      </div>
    </div>

  )
}

export default MyPosts;