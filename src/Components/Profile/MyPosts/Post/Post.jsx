import React from 'react';
import styles from './Post.module.css';

const Post = (props) => {
  return (
    <div className={styles.item}>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Gull_portrait_ca_usa.jpg/300px-Gull_portrait_ca_usa.jpg" />
      {props.postText}
      <div>
        <span>Like</span> {props.likeCount}
      </div>
    </div>
  )
}

export default Post;