import React from 'react';
import MyPostsContainer from './MyPosts/MyPostsContainer';
import prom from './Profile.module.css';
import ProfileInfo from './ProfileInfo/ProfileInfo';

const Profile = (props) => {
  console.log(props)
  return (
    <div className={prom.profile_wrap}>
      <ProfileInfo profile={props.profile} status={props.status} updateUserStatus={props.updateUserStatus}/>
      <MyPostsContainer />
    </div>
  )
}

export default Profile;