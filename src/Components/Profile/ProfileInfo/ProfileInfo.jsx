import React from 'react';
import Preloader from '../../common/Preloader/Preloader';
import styles from './ProfileInfo.module.css';
import userAvatar from "./../../../assets/images/users2.png";
import ProfileStatus from './ProfileStatus';

const ProfileInfo = (props) => {
  if (!props.profile) {
    return <Preloader />
  }
  return (
      <div className={styles.info_block_set}>
        <div className={styles.fullName}>
          {props.profile.fullName}
        </div>
        <div className={styles.avatar_set}>
          <img src={props.profile.photos.large != null ? props.profile.photos.large : userAvatar} />
        </div>
        <div className={styles.fullName}>
          Статус:
          <div>
            <ProfileStatus status={props.status} updateUserStatus={props.updateUserStatus} />
          </div>
        </div>
      </div>
  )
}

export default ProfileInfo;