import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Profile from './Profile';
import { getUserProfile, getUserStatus, updateUserStatus } from '../../Redux/Profile-Reducer';
import { useParams } from 'react-router-dom';
import { withAuthRedirect } from '../../hoc/Auth-Redirect';
import { compose } from 'redux';


const ProfileContainer = (props) => {

    let { userId } = useParams();
    if (!userId) {
        userId = props.authorizedUserId;
        if(!userId){
            props.userId.history.push("/login")
        }
    }

    useEffect(() => {
        props.getUserProfile(userId);
        props.getUserStatus(userId);
    }, [userId]);

    return (
        <Profile {...props} profile={props.profile} status={props.status} updateUserStatus={props.updateUserStatus} />
    )

}

let mapStateToProps = (state) => ({
    profile: state.profilePage.profile,
    status: state.profilePage.status,
    authorizedUserId: state.auth.userId,
    isAuth: state.auth.isAuth,
})


export default compose(
    connect(mapStateToProps, {getUserProfile, getUserStatus, updateUserStatus }),
    withAuthRedirect
)(ProfileContainer);