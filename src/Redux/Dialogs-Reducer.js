const SEND_MESSAGE = 'SEND-MESSAGE';

let initialState = {
    dialogs: [
        { id: 1, name: 'Паша' },
        { id: 2, name: 'Кися' },
        { id: 3, name: 'Леня' },
        { id: 4, name: 'Филя' },
        { id: 5, name: 'Степашка' },
        { id: 6, name: 'Ворона' }
    ],
    messages: [
        { id: 1, message: 'Hi' },
        { id: 2, message: 'Как ты?' },
        { id: 3, message: 'Я в говно' },
        { id: 4, message: 'Я в говно' },
        { id: 5, message: 'Я в говно' },
        { id: 6, message: 'Я в говно' }
    ]
}

const dialogsReducer = (state = initialState, action) => {

    switch (action.type) {
        case SEND_MESSAGE:
            let newMessage = action.newMessageText;
            return {
                ...state,
                messages: [...state.messages, { id: 7, message: newMessage }]
            }
        default:
            return state;
    }

}

export const sendMessageAC = (newMessageText) => ({ type: SEND_MESSAGE, newMessageText})

export default dialogsReducer;