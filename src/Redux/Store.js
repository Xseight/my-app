import dialogsReducer from "./Dialogs-Reducer";
import profileReducer from "./Profile-Reducer";
import sidebarReducer from "./Sidebar-Reducer";

let store = {
    _state: {
        profilePage: {
            posts: [
                { id: 1, message: 'Привет, как ты?', likesCount: 12 },
                { id: 2, message: 'Это мой первый пост', likesCount: 11 },
                { id: 3, message: 'ХАХАХАХ', likesCount: 11 },
                { id: 4, message: 'ОХОХОХ', likesCount: 11 }
            ],
            newPostText: 'Что нового?'
        },

        dialogsPage: {
            dialogs: [
                { id: 1, name: 'Паша' },
                { id: 2, name: 'Кися' },
                { id: 3, name: 'Леня' },
                { id: 4, name: 'Филя' },
                { id: 5, name: 'Степашка' },
                { id: 6, name: 'Ворона' }
            ],
            messages: [
                { id: 1, message: 'Hi' },
                { id: 2, message: 'Как ты?' },
                { id: 3, message: 'Я в говно' },
                { id: 4, message: 'Я в говно' },
                { id: 5, message: 'Я в говно' },
                { id: 6, message: 'Я в говно' }
            ],
            newMessageText: ''
        },
        sidebar: {}
    },
    _callSubscriber() {
        console.log('state changed');
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {

        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);
        this._callSubscriber(this._state); 

    }

}

export default store;
window.store = store;