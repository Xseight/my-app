import {usersAPI} from "../api/api";

const ADD_FRIEND = 'ADD-FRIEND';
const DELETE_FRIEND = 'DELETE-FRIEND';
const SET_USERS = 'SET-USERS';
const SET_CURRENT_PAGE = 'SET-CURRENT-PAGE';
const SET_USERS_TOTAL_COUNT = 'SET-USERS-TOTAL-COUNT';
const TOGGLE_IS_FETCHING = 'TOGGLE-IS-FETCHING';
const TOGGLE_FOLLOWING_IN_PROGRESS = 'TOGGLE_FOLLOWING_IN_PROGRESS';

let initialState = {
    users: [ ],
    pageSize: 5,
    totalUserCount: 0,
    currentPage: 1,
    isFetching: false,
    followingInProgress: []
}

//create action -->

const usersReducer = (state = initialState, action) => {
    switch (action.type) {

        case ADD_FRIEND: {
            return {...state, users: state.users.map(u => {
                    if (u.id === action.userId) {
                        return {
                            ...u,
                            followed: true
                        }
                    }
                    return u;
                })
            }
        }
        case DELETE_FRIEND: {
                return {...state, users: state.users.map(u => {
                        if (u.id === action.userId) {
                            return {
                                ...u,
                                followed: false
                            }
                        }
                        return u;
                    })
            }
        }
        case SET_USERS: {
                return {...state, users: action.users}
        }
        case SET_CURRENT_PAGE: {
                    return {...state, currentPage: action.currentPage}
        }
        case SET_USERS_TOTAL_COUNT: {
                    return {...state, totalUserCount: action.count}
        }
        case TOGGLE_IS_FETCHING: {
                    return {...state, isFetching: action.isFetching}
        }
        case TOGGLE_FOLLOWING_IN_PROGRESS: {
                    return {...state, followingInProgress: action.isFetching 
                        ? [...state.followingInProgress, action.userId] 
                        : state.followingInProgress.filter(id => id != action.userId)
                    }
        }

        default:
            return state;
    }
}

//action creator's -->

export const acceptAddFriend = (userId) => ({type: ADD_FRIEND, userId});
export const acceptDeleteFriend = (userId) => ({type: DELETE_FRIEND, userId});
export const setUsers = (users) => ({type: SET_USERS, users});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setUsersTotalCount = (totalUsersCount) => ({type: SET_USERS_TOTAL_COUNT, count: totalUsersCount})
export const toggleIsFetching = (isFetching) => ({type: TOGGLE_IS_FETCHING, isFetching});
export const toggleFollowingInProgress = (isFetching, userId) => ({type: TOGGLE_FOLLOWING_IN_PROGRESS, isFetching, userId});

//ThunkCreator's -->

export const getUsers = (currentPage, pageSize) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true));
        usersAPI.getUsers(currentPage, pageSize).then(data => {
            dispatch(toggleIsFetching(false));
            dispatch(setUsers(data.items));
            dispatch(setUsersTotalCount(data.totalCount / 100));
            dispatch(setCurrentPage(currentPage));
        })
    }
}

export const addFriend = (userId) => {
    return (dispatch) => {
        dispatch(toggleFollowingInProgress(true, userId));
        usersAPI.addFriend(userId)
            .then(response => {
                if (response.data.resultCode === 0) {
                    dispatch(acceptAddFriend(userId));
                }
                dispatch(toggleFollowingInProgress(false, userId));
            });
    }
}

export const deleteFriend = (userId) => {
    return (dispatch) => {
        dispatch(toggleFollowingInProgress(true, userId));
        usersAPI.deleteFriend(userId).then(response => {
            if (response.data.resultCode === 0) {
                dispatch(acceptDeleteFriend(userId));
            }
            dispatch(toggleFollowingInProgress(false, userId));
        });
    }
}

export default usersReducer;