import { profileAPI } from "../api/api";

const ADD_POST = 'ADD-POST';
const SET_USER_PROFILE = 'SET-USER-PROFILE';
const SET_USER_STATUS = 'SET-USER-STATUS'

let initialState = {
    posts: [
        { id: 1, postText: 'Привет, как ты?', likesCount: 12 },
        { id: 2, postText: 'Это мой первый пост', likesCount: 11 },
    ],
    profile: null,
    status: "",
}

//create Actions---->

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:{
            let newPostText = action.newPostText;
            return {
                ...state,
                posts: [...state.posts, { id: 5, postText: newPostText, likesCount: 0 }],
            }
        }
        case SET_USER_PROFILE: {
            return {
                ...state,
                profile: action.profile
            }
        }
        case SET_USER_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }
        default:
            return state;
    }



}
//Create Action Creator's --->

export const addPostAC = (newPostText) => ({ type: ADD_POST, newPostText });

export const setUserProfile = (profile) => ({ type: SET_USER_PROFILE, profile})

export const setUserStatus = (status) => ({ type: SET_USER_STATUS, status})

//thunk creator's --->

export const getUserProfile = (userId) => {
    return (dispatch) => {
        profileAPI.getUserProfile(userId).then(response => {
           dispatch(setUserProfile(response.data))
        })
    }
}

export const getUserStatus = (userId) => {
    return (dispatch) => {
        profileAPI.getUserStatus(userId).then(response => {
           dispatch(setUserStatus(response.data))
        })
    }
}

export const updateUserStatus = (status) => {
    return (dispatch) => {
        profileAPI.updateUserStatus(status).then(response => {
            if (response.data.resultCode === 0){
           dispatch(setUserStatus(status))
            }
        })
    }
}


export default profileReducer;