import { checkAuthUser } from "./auth-reducer";

const INITIALIZED_SUCCESS = 'INITIALIZED_SUCCESS';


let initialState = {
    initialized: false,
}
//create action --->
const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIALIZED_SUCCESS: {
            return { ...state, initialized: true }
        }
        default:
            return state;
    }
}

//action creator --->
export const initializedSuccess = () => ({ type: INITIALIZED_SUCCESS });

//thunk creator's --->

export const initializeApp = () => (dispatch) => {
        let promise = dispatch(checkAuthUser());
        Promise.all([promise])
            .then( () => {
            dispatch(initializedSuccess());
        });
    }

export default appReducer;