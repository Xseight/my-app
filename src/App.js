import './App.css';
import Navbar from './Components/Navbar/Navbar';
import { Route, Routes } from 'react-router-dom';
import DialogsContainer from './Components/Dialogs/DialogsContainer';
import UsersContainer from './Components/Users/users-container';
import ProfileContainer from './Components/Profile/ProfileContainer';
import HeaderContainer from './Components/Header/Header-Container';
import Login from './Components/Login/login';
import React, { useEffect } from 'react';
import { initializeApp } from './Redux/app-reducer';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Preloader from './Components/common/Preloader/Preloader';


class App extends React.Component {

  componentDidMount() {
    this.props.initializeApp()
  }
  render() {
    if (!this.props.initialized){
      return <Preloader />
    }

    return (

      <div className='app-wrapper'>
        <HeaderContainer />
        <Navbar />
        <div className='app-wrapper-content'>
          <Routes>
            <Route path="/profile" exact element={<ProfileContainer />} />
            <Route path="/profile/:userId" element={<ProfileContainer />} />
            <Route path="/message" element={<DialogsContainer />} />
            <Route path="/users" element={<UsersContainer />} />
            <Route path="/login" element={<Login />} />
          </Routes>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized
})

export default compose
(connect(mapStateToProps, { initializeApp }))(App);
